using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;
class Component : VisualElement {
  protected VisualTreeAsset template;
  
  protected object _model;
  public string _modelKey { get; set; }
  public new class UxmlFactory : UxmlFactory<Component, UxmlTraits> {}
  public new class UxmlTraits : VisualElement.UxmlTraits {
    UxmlStringAttributeDescription _modelKey = new UxmlStringAttributeDescription { name = "data" };
    public override IEnumerable<UxmlChildElementDescription> uxmlChildElementsDescription {
      get { yield break; }
    }
    public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc) {
      base.Init(ve, bag, cc);
      ((Component)ve)._modelKey = _modelKey.GetValueFromBag(bag, cc);
    }
  }
  public void Bind(object model){
    if (model != null){
      this._model = model;
      BindValues();
      BindEvents();
    }
  }
  private void BindValues(){
    if (!String.IsNullOrEmpty(this._modelKey)){
      var p = this._model.GetType().GetProperty(this._modelKey);
      if(p != null){
        Debug.Log(p.GetValue(this._model));
        return;
      }
    }
  }
  private void BindEvents(){

  }

  protected void LoadTemplate(string path)
  {
    this.Clear();
    template = Resources.Load<VisualTreeAsset>(path);
    if(template != null) {
      template.CloneTree(this);
      return;
    }
    Debug.Log($"Failed loading template for Component: {this.GetType().Name} from {path}");
  }
}