using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UIElements;

public class MusicSelect : MonoBehaviour {
  private DBLayerORM db;
  private int SelectedSeries = -1;
  private List<Series> series;
  private VisualElement doc;
  public string ViewTitle { get; set; } = "Song list";
  public void Awake() {
    db = Object.FindObjectOfType<DBLayerORM>();
    doc = this.GetComponent<UIDocument>().rootVisualElement;
    doc.Q<Component>().Bind(this);
    series = db.GetSeries();
    // ListSongs(SelectedSeries);
  }
  private void ListSongs(int id = -1) {
    List<Song> songs;
    if (id < 0) {
      // get all songs from DB
      songs = db.GetAllSongs();
    } else {
      // get songs from series
      songs = db.GetSongsFromSeries(id);
    }
    songs.ForEach(delegate(Song song){
    });
  }
}