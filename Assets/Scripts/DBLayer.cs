/* using UnityEngine;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using Mono.Data.Sqlite;

public class DBLayer : MonoBehaviour {
  private static string DBPath = "";
  private static IDbConnection _db;
  private static DBLayer _instance;
  public static DBLayer Instance => _instance;
  public bool fastLoad;
	private void Awake() {
		if (_instance == null) {
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			_instance = this;
      Setup();
		}
		else {
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}
  private void Setup(){
    DBPath = $"{Directory.GetCurrentDirectory()}/songs.db";
    if(!fastLoad) DestroyDB();
    ConnectDB();
    InitDB();
  }
  private void DestroyDB(){
      if(File.Exists(DBPath)){
          File.Delete(DBPath);
      }
  }
  private void ConnectDB(){
      _db = new SqliteConnection($"URI=file:{DBPath}");
      _db.Open();
  }
  private void InitDB(){
    ExecuteNonQuery(@"CREATE TABLE IF NOT EXISTS groups (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      title VARCHAR(255),
      fullTitle VARCHAR(255),
      dirname VARCHAR(255) UNIQUE,
      cover VARCHAR(255),
      banner VARCHAR(255)
    );
    CREATE TABLE IF NOT EXISTS songs (
      id INTEGER PRIMARY KEY AUTOINCREMENT,
      group_id INTEGER,
      title VARCHAR(255),
      fullTitle VARCHAR(255),
      type INTEGER,
      sort INTEGER,
      FOREIGN KEY(group_id) REFERENCES groups(id)
    );");
  }

  public string[] GetDBGroupDirs(){
    var dbcmd = _db.CreateCommand();
    dbcmd.CommandText = @"SELECT dirname FROM groups;";
    var reader = dbcmd.ExecuteReader();
    List<string> groups = new List<string>();
    while(reader.Read()){
      groups.Add((string)reader[0]);
    }
    return groups.ToArray();
  }
  public int InsertGroup(string path, GroupInfo info){
    var dirname = Path.GetFileName(path);
    var cmd = _db.CreateCommand();
    cmd.CommandText = $@"INSERT INTO groups (
      title,
      fullTitle,
      dirname
    ) VALUES (
      '{info.title}',
      '{info.fullTitle}',
      '{dirname}'
    );
    SELECT SEQ from sqlite_sequence WHERE name='groups';";
    var r = cmd.ExecuteReader();
    r.Read();
    return r.GetInt32(0);
  }

  public int InsertSong(string path, SongInfo info){
    return -1;
  }

  public List<GroupInfo> GetGroups(){
    var cmd = _db.CreateCommand();
    cmd.CommandText = @"SELECT dirname FROM groups;";
    var reader = cmd.ExecuteReader();
    List<GroupInfo> groups = new List<GroupInfo>();
    while(reader.Read()){
      var group = new GroupInfo();
      group.id = reader.GetInt32(0);
      group.title = reader.GetString(1);
      group.fullTitle = reader.GetString(2);
      group.dirname = reader.GetString(3);
    }
    return groups;
  }

  public List<SongInfo> GetSongsByGroup(int id){
    return ExecuteReader($"SELECT * FROM songs WHERE group_id = {id};")
    .Select(row => {
      SongInfo info = new SongInfo();
      info.id = (int)row[0];
      info.group_id = (int)row[1];
      info.title = (string)row[2];
      info.fullTitle = (string)row[3];
      return info;
    })
    .ToList();
  }
  public List<SongInfo> GetSongsByGroup(GroupInfo info){
    return GetSongsByGroup(info.id);
  }

 	private void OnDestroy() {
		if (_instance == this) {
			_instance = null;
		}
	}

  private IDbCommand CreateCommand(string query){
    var cmd = _db.CreateCommand();
    cmd.CommandText = query;
    return cmd;
  }
  private void ExecuteNonQuery(string query){
    CreateCommand(query).ExecuteNonQuery();
  }

  private List<object[]> ExecuteReader(string query){
    IDataReader reader = CreateCommand(query).ExecuteReader();
    List<object[]> rows = new List<object[]>();
    while (reader.Read()){
      object[] row = new object[reader.FieldCount];
      for (int i = 0; i < reader.FieldCount; i++){
          row[i] = reader[i];
      }
      rows.Add(row);
    }
    return rows;
  }

  // private List<T> GetRows<T>(string tableName)  where T : new() {
  //   IEnumerable<string> propertyNames = typeof(T)
  //                                       .GetProperties()
  //                                       .ToList()
  //                                       .Select(property => property.Name);

  //   var cmd = _db.CreateCommand();
  //   cmd.CommandText = $@"SELECT * FROM {tableName}";
  //   var reader = cmd.ExecuteReader();

  //   List<string> fields = new List<string>();
  //   for ( int i = 0; i < reader.FieldCount; i++) {
  //     fields.Add(reader.GetName(i));
  //   }

  //   fields = fields.Where(name => propertyNames.Contains(name)).ToList();

  //   List<T> rows = new List<T>();
  //   while(reader.Read()){
  //     T model = new T();
  //     fields.ForEach(delegate(string field){

  //     });
  //   }
  //   return rows;
  // }
} */