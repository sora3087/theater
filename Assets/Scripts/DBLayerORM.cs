using UnityEngine;
using System;
// using System.Data;
using System.IO;
using System.Reflection;
// using System.Linq;
using System.Collections.Generic;
using SQLite;

class Sequence {
  public int seq { get; set; }
}
public class DBLayerORM : MonoBehaviour {
  private static string DBPath = "";
  private static DBLayerORM _instance;
  public static DBLayerORM Instance => _instance;
  private static SQLiteConnection _db;
  [SerializeField]
  private bool fastLoad;
	private void Awake() {
    fastLoad = GameConstants.FastLoad;
    if (_instance == null) {
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			_instance = this;
      Setup();
		}
		else {
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}
  private void Setup() {
    DBPath = $"{Directory.GetCurrentDirectory()}/songs.db";
    if(!fastLoad) DestroyDB();
    ConnectDB();
    InitDB();
  }
  private void DestroyDB() {
    if(File.Exists(DBPath)) {
      File.Delete(DBPath);
    }
  }
  private void ConnectDB() {
    _db = new SQLiteConnection(DBPath);
  }
  private void InitDB() {
    _db.CreateTable<Series>();
    _db.CreateTable<Song>();
  }
  private int getLastId(string tableName) {
    var results = _db.Query<Sequence>($"SELECT seq FROM sqlite_sequence WHERE name = '{tableName}' LIMIT 0,1");
    if(results.Count > 0){
      return results[0].seq;
    }
    return 0;
  }
  public int InsertSeries(Series series) {
    _db.Insert(series);
    return getLastId("series");
  }
  public int InsertSong(Song song){
    _db.Insert(song);
    return getLastId("songs");
  }
  public string GetTableName(Type type) {
    TableAttribute table =
            (TableAttribute) Attribute.GetCustomAttribute(type, typeof(TableAttribute));
    if (table == null) {
      return type.Name;
    } else {
      return table.Name;
    }
  }
  public int InsertRecord<T>(T record) where T : Record{
    _db.Insert(record);
    Type type = typeof(T);

    return getLastId(GetTableName(type));
  }

  public void InsertRecords<T>(List<T> records) where T : Record {
    records.ForEach((T record) => {
      // is record a reference records[i]?
      record.id = InsertRecord(record);
    });
  }

  public void DeleteRecords<T>(List<T> records) where T : Record{
    records.ForEach(delegate(T record){
      if(record.id >= 0)
        _db.Delete<Record>(record.id);
    });
  }

  public List<Series> GetSeries() {
    return _db.Table<Series>().ToList();
  }
  public List<Song> GetAllSongs() {
    return _db.Table<Song>().ToList();
  }
  public List<Song> GetSongsFromSeries(int id) {
    var query = new TableQuery<Song>(_db);
    query = query.Where((song) => song.seriesId == id);
    return query.ToList();
  }

  public void DeleteSeries(List<Series> toDelete){
    toDelete.ForEach(delegate(Series series){
      if(series.id >= 0)
        _db.Delete<Series>(series.id);
    });
  }
  public void InsertSeries(List<Series> toInsert){
    _db.InsertAll(toInsert);
  }

  public void OnDestroy(){
    _db.Close();
  }
  public void OnApplicationQuit() {
    _db.Close();
  }
}