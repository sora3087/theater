using SQLite;

[Table("songs")]
public class Song : Record {
  public int seriesId { get; set; }
  public ChartType type { get; set; }
  public string audio { get; set; }
}