using SQLite;
public abstract class Record {
  [PrimaryKey, AutoIncrement]
  public int id { get; set; } = -1;
  [Unique]
  public string path { get; set; }
  public int sort { get; set; }
  public string title { get; set; }
  public string fullTitle { get; set; }
  public string hero { get; set; }
}