﻿using System.IO;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using Newtonsoft.Json;

public class RecordComparer : IEqualityComparer<Record> {
  public bool Equals(Record a, Record b) {
    return a.path == b.path;
  }

  public int GetHashCode(Record record) => record.path.GetHashCode();
}

public class DirectoryLoader : MonoBehaviour {
  private DBLayerORM db;
  string songsPath;
  [SerializeField]
  private bool fastLoad;
  public void Awake() {
    fastLoad = GameConstants.FastLoad;
    Debug.Log("DirectoryLoader Awake");

    // use a path at the root of the program directory
    songsPath = $"{Directory.GetCurrentDirectory()}\\Songs";

    // get DB object for cached lookups
    db = Object.FindObjectOfType<DBLayerORM>();

    // find all series folders in Songs directory
    List<string> seriesDirs = GetRelativeChildDirectories(songsPath);

    //-- load series --//
    // if fastload
    List<Series> dbSeries = new List<Series>();
    if (fastLoad) {
      // load all series from DB first to compare if they need to be loaded
      dbSeries = db.GetSeries();
    }
    // if not fastload
      // db is already destroyed, loading db series will result in an empty list

    // filter filesystem series by db series
      // if there are no db series then the original list of filesystem series are loaded

    List<Series> newSeries = GetSeriesMeta(FilterPathsByRecords(seriesDirs, dbSeries));
    // take filtered list of series and insert them into the database
      // only use validated series for inserting

    db.InsertRecords(newSeries);

    // either way, filter list of db series by filesystem
    // remove list of resulting db series as they aren't in the filesystem

    db.DeleteRecords(FilterRecordsByPaths(dbSeries, seriesDirs));

    //-- load songs --//
    dbSeries = db.GetSeries();

    // iterate all DB series and compare songs per series
    dbSeries.ForEach((Series series) => {
      List<string> songDirs = GetRelativeChildDirectories($"{songsPath}\\{series.path}").ToList();
      List<Song> dbSongs = new List<Song>();
      if(fastLoad) {
        dbSongs = db.GetSongsFromSeries(series.id);
      }

      List<Song> newSongs = GetSongMeta(FilterPathsByRecords(songDirs, dbSongs), series.id);

      db.InsertRecords(newSongs);

      var deleteList = FilterRecordsByPaths(dbSongs, songDirs);
      db.DeleteRecords(deleteList);
    });

    Debug.Log("DirectoryLoader: done");
  }
  private List<string> GetRecordPaths<T>(List<T> records) where T : Record{
    return records.Select(record => record.path).ToList();
  }
  private List<string> FilterPathsByRecords<T>(List<string> strings, List<T> dbRecords) where T : Record{
    // filter the list of series
    List<string> dbPaths = GetRecordPaths(dbRecords); // potentially empty
    return strings.Where(str => !dbPaths.Any(dbPath => dbPath == str)).ToList();
  }
  private List<T> FilterRecordsByPaths<T>(List<T> dbRecords, List<string> paths) where T : Record{
    return dbRecords.Where(record => !paths.Any(path => path == record.path)).ToList();
  }
  private Series GetSeriesMeta(string seriesPath){
    Series model = new Series();
    string jsonPath = $"{seriesPath}\\series.json";

    using (StreamReader r = new StreamReader(jsonPath)){
      string json = r.ReadToEnd();
      model = JsonConvert.DeserializeObject<Series>(json);
      model.path = Path.GetFileName(seriesPath);
      if(model.fullTitle == "") {
        // fallback to title if fullTitle is missing
        model.fullTitle = model.title;
      }
      if(model.hero == "") {
        // missing banner file try to find one in the directory
        string[] heroFiles = Directory.GetFiles($"{seriesPath}\\hero.*");
        if(heroFiles.Length > 0){
          model.hero = Path.GetFileName(heroFiles[0]);
        }
      }
      if(model.icon == "") {
        // missing cover file try to find one in the directory
        string[] iconFiles = Directory.GetFiles($"{seriesPath}\\icon.*");
        if(iconFiles.Length > 0){
          model.hero = Path.GetFileName(iconFiles[0]);
        }
      }
    }

    return model;
  }
  
  private List<Series> GetSeriesMeta (List<string> paths){
    List<Series> meta = new List<Series>();

    paths.ForEach(delegate(string path){
      string seriesPath = $"{songsPath}\\{path}";
      
      if(File.Exists($"{seriesPath}\\series.json")) {
        meta.Add(GetSeriesMeta(seriesPath));
      }
    });

    return meta;
  }

  private List<string> GetRelativeChildDirectories(string dir){
    return Directory.GetDirectories(dir).Select(child => child.Replace(songsPath + "\\", "")).ToList();
  }
  private List<Song> GetSongMeta(List<string> songPaths, int seriesId){
    List<Song> meta = new List<Song>();
    songPaths.ForEach(delegate(string path){
      string songPath = $"{songsPath}\\{path}";
      string jsonPath = $"{songPath}\\song.json";
      if (File.Exists(jsonPath)){
        using (StreamReader r = new StreamReader(jsonPath)){
          string json = r.ReadToEnd();

          Song model = JsonConvert.DeserializeObject<Song>(json);
          if(model.audio == null || model.audio == ""){
            model.audio = "music.dspadpcm.wav";
          }
          if(File.Exists($"{songPath}\\{model.audio}")){
            model.seriesId = seriesId;
            model.path = path;
            meta.Add(model);
          } else {
            // no audio stream could be found so dont add the song to the index
            return;
          }
        }
      }
    });
    return meta;
  }
  private List<string> GetAllSongDirs(List<Series> seriesList) {
    List<string> ret = new List<string>();
    seriesList.ForEach(delegate(Series series){
      string seriesPath = $"{songsPath}\\{series.path}";
      ret = ret.Concat(GetRelativeChildDirectories(seriesPath)).ToList();
    });
    return ret;
  }
}
