using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using Newtonsoft.Json;
using SQLite;

public enum ChartType{
  FMS = 0,
  BMS = 1,
  EMS = 2
}

public enum TriggerType{
  Tap,
  Slide,
  HoldStart,
  HoldNode,
  HoldEnd,
  HoldEndSlide
}
public class Trigger{
  public uint time;
  public TriggerType type;
  public int x; // only used in EMS
  public int y; // in BMS y is the lane number
  public uint angle;
  public uint unknown;
}
[Table("charts")]
public class Chart{
  public int songID { get; set; } = 0;
  public ChartType type { get; set; } = ChartType.BMS;
  public uint triggerCount { get; set; } = 0;
  public uint duration { get; set; } = 0;
  public uint duration2 = 0;
  public uint featureStart { get; set; } = 0;
  public uint featureEnd { get; set; } = 0;
  public uint summonStart { get; set; } = 0;
  public uint summonEnd { get; set; } = 0;
  public uint summonAttack { get; set; } = 0;
  public List<Trigger> triggers;

  Chart(){}

  public static Chart fromJson(string path){
    var chart = new Chart();
    return chart;
  }
  public static Chart fromBytes(string path, bool readTriggers = true){
    /* byte file header */
    /**
      uint32 chartType
      uint32 duration
      uint32 unkown
      uint32 duration2 (always the same as duration)
      uint32 featureStart
      uint32 featureEnd
      uint32 summonStart
      uint32 summonEnd
      uint32 summonAttack
    */
    var chart = new Chart();
    using (BinaryReader reader = new BinaryReader(File.Open(path, FileMode.Open))){
      chart.type = (ChartType)reader.ReadUInt32();
      chart.duration = reader.ReadUInt32();
      reader.ReadUInt32(); // skip unknown 
      chart.duration2 = reader.ReadUInt32();
      chart.featureStart = reader.ReadUInt32();
      chart.featureEnd = reader.ReadUInt32();
      chart.summonStart = reader.ReadUInt32();
      chart.summonEnd = reader.ReadUInt32();
      chart.summonAttack = reader.ReadUInt32();
      chart.triggerCount = reader.ReadUInt32();
      if(readTriggers) {
        chart.triggers = new List<Trigger>();
        for (int i = 0; i < chart.triggerCount; i++) {
          byte[] triggerBytes = reader.ReadBytes(0x18);
          var trigger = new Trigger();
          trigger.time = BitConverter.ToUInt32(triggerBytes, 0x0);
          trigger.type = (TriggerType)BitConverter.ToUInt32(triggerBytes, 0x4);
          trigger.x = BitConverter.ToInt32(triggerBytes, 0x8);
          trigger.y = BitConverter.ToInt32(triggerBytes, 0xC);
          trigger.angle = BitConverter.ToUInt32(triggerBytes, 0x10);
          trigger.unknown = BitConverter.ToUInt32(triggerBytes, 0x14);
        }
      }
    }
    return chart;
  }
}