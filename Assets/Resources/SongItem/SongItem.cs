using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UIElements;
class SongItem : Component {
  public new class UxmlFactory : UxmlFactory<SongItem, UxmlTraits> {}
  public new class UxmlTraits : VisualElement.UxmlTraits {
    public override void Init(VisualElement ve, IUxmlAttributes bag, CreationContext cc) {
      base.Init(ve, bag, cc);
      ((SongItem)ve).LoadTemplate("SongItem/SongItem");
    }
  }
}